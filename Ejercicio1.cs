using System;
using System.Collections.Generic;
using System.Linq;

namespace Practica
{
    class Program
    {
        static void Main(string[] args)
        {
            //Monedas y Billetes
            int[] Dinero = new int[] { 5, 10, 25, 50, 100, 200 };

            
            List<Productos> productos = new List<Productos>();
            int[] ID = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            string[] Nombres = { "Jugo de Naranja","Agua","Refresco","Galletica salada","Manzana","Chocolate","Papita","Chicles","Palomita","Chocolate"};
            int[] Precios = { 15, 15, 20, 20, 30, 35, 40, 40, 10, 25 };
            int[] Existencia = { 5, 3, 1, 5, 10, 3, 2, 9, 10, 5 };

            for (int i = 0; i < Nombres.Length; i++)
            {
                productos.Add(new Productos() { ID = ID[i], Nombre = Nombres[i], Precio = Precios[i], Existencia = Existencia[i] });

            }

            Console.WriteLine("---------------------------------------");
            Console.WriteLine("--        MAQUINA DE PRODUCTO        --");
            Console.WriteLine("---------------------------------------");

            Console.WriteLine();
            foreach (Productos Articulos in productos)
            {
                Console.WriteLine($"{Articulos.ID}  - Articulo: {Articulos.Nombre} -- Precio: {Articulos.Precio}");
            }
            Console.WriteLine("---------------------------------------");
            Console.Write("Introduce el dinero: ");

            try {
                int DineroIngresado = int.Parse(Console.ReadLine());
                int DineroTotal = 0;

                if (Dinero.Contains(DineroIngresado))
                {
                    DineroTotal = DineroIngresado;
                    Console.WriteLine("");
                    Console.Write("Eligir el Articulo a consumir: ");
                    int opcion = int.Parse(Console.ReadLine());
                    var data = productos.Where(x => x.ID == opcion).ToList();
                    Console.WriteLine();
                    foreach (var i in data)
                    {
                        Console.WriteLine($"{i.Nombre} -- {i.Precio}$ ");
                        Console.WriteLine("");
                        Console.WriteLine("-------------");
                        Console.WriteLine("1 - Confirmar");
                        Console.WriteLine("2 - Cancelar");
                        Console.WriteLine("-------------");
                        Console.Write("Eligir una opcion: ");
                        int confirmar = int.Parse(Console.ReadLine());
                        if (confirmar == 1)
                        {
                            Console.WriteLine("");
                            int compra = ( DineroTotal - i.Precio);
                            if (compra >= 0)
                            {
                                int DineroRestante = (DineroTotal - i.Precio);

                                Console.WriteLine($"Compra confirmada, dinero total: {DineroRestante}");
                                Console.WriteLine($"Articulo elegido: {i.Nombre}");
                                i.ID = (i.ID - 1);
                                Console.WriteLine($"Existencia: {i.ID}");
                            }
                            else
                            {
                                Console.WriteLine("Dinero insuficiente");
                            }
                        }
                        else
                        {
                            Console.WriteLine($"Cancelando compra y regresando el dinero: {DineroTotal}$");
                        }
                    }

                }
                else
                {
                    Console.WriteLine("No se admite ese tipo de Dinero");
                }
            }
           
            catch (Exception e) 
            {
                Console.WriteLine("valor invalido");
            }
            Console.ReadKey();
        }

        public class Productos
        {
            public int ID { get; set; }
            public string Nombre { get; set; }
            public int Precio { get; set; }
            public int Existencia { get; set; }

        }
    }
}
